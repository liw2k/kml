scholastic_administrators = {

	monarch_power = ADM

		potential = {
		has_idea_group = theocratic_ideas
		has_idea_group = administrative_ideas
		OR = {
			full_idea_group = theocratic_ideas
			full_idea_group = administrative_ideas
		}	
	}
	allow = {
		full_idea_group = theocratic_ideas
		full_idea_group = administrative_ideas
	}
	
	state_maintenance_modifier = -0.25
	
	ai_will_do = {
		factor = 1	
	}
}

prof_rel = {
	monarch_power = ADM

		potential = {
		has_idea_group = professional_army_ideas
		has_idea_group = religious_ideas
		OR = {
			full_idea_group = professional_army_ideas
			full_idea_group = religious_ideas
		}	
	}
	allow = {
		full_idea_group = professional_army_ideas
		full_idea_group = religious_ideas
	}
	
	land_attrition = -0.15
	
	
	ai_will_do = {
		factor = 1	
	}
}	

prof_eco = {
	monarch_power = ADM

		potential = {
		has_idea_group = professional_army_ideas
		has_idea_group = economic_ideas
		OR = {
			full_idea_group = professional_army_ideas
			full_idea_group = economic_ideas
		}	
	}
	allow = {
		full_idea_group = professional_army_ideas
		full_idea_group = economic_ideas
	}
	
	land_maintenance_modifier = -0.05
	
	ai_will_do = {
		factor = 1	
	}
}

prof_admin = {
	monarch_power = ADM

		potential = {
		has_idea_group = professional_army_ideas
		has_idea_group = administrative_ideas
		OR = {
			full_idea_group = professional_army_ideas
			full_idea_group = administrative_ideas
		}	
	}
	allow = {
		full_idea_group = professional_army_ideas
		full_idea_group = administrative_ideas
	}
	
	reinforce_speed = 0.25
	
	ai_will_do = {
		factor = 1	
	}
}

church_coffers = {
	monarch_power = ADM

		potential = {
		has_idea_group = theocratic_ideas
		has_idea_group = economic_ideas
		OR = {
			full_idea_group = theocratic_ideas
			full_idea_group = economic_ideas
		}	
	}
	allow = {
		full_idea_group = theocratic_ideas
		full_idea_group = economic_ideas
	}
	
	interest = -0.5
	papal_influence = 0.5
	
	ai_will_do = {
		factor = 1	
	}
}

pilgrim_minorities = {
	monarch_power = ADM

		potential = {
		has_idea_group = theocratic_ideas
		has_idea_group = expansion_ideas
		OR = {
			full_idea_group = theocratic_ideas
			full_idea_group = expansion_ideas
		}	
	}
	allow = {
		full_idea_group = theocratic_ideas
		full_idea_group = expansion_ideas
	}
	
	colonist_placement_chance = 0.05
	global_colonial_growth = 10
	ai_will_do = {
		factor = 1	
	}
}

blooming_intellectuals = {
	monarch_power = ADM

		potential = {
		has_idea_group = theocratic_ideas
		has_idea_group = innovativeness_ideas
		OR = {
			full_idea_group = theocratic_ideas
			full_idea_group = innovativeness_ideas
		}	
	}
	allow = {
		full_idea_group = theocratic_ideas
		full_idea_group = innovativeness_ideas
	}
	
	global_institution_spread = 0.1
	technology_cost = -0.05
	ai_will_do = {
		factor = 1	
	}
}
	
	
the_combination_act = {

	monarch_power = ADM

	potential = {
		has_idea_group = aristocracy_ideas
		has_idea_group = innovativeness_ideas
		OR = {
			full_idea_group = aristocracy_ideas
			full_idea_group = innovativeness_ideas
		}	
	}
	allow = {
		full_idea_group = aristocracy_ideas
		full_idea_group = innovativeness_ideas
	}
	
	production_efficiency = 0.15
	
	ai_will_do = {
		factor = 1	
	}
}

the_court_of_wards_and_liveries = {

	monarch_power = ADM

	potential = {
		has_idea_group = aristocracy_ideas
		has_idea_group = economic_ideas
		OR = {
			full_idea_group = aristocracy_ideas
			full_idea_group = economic_ideas
		}		
	}
	allow = {
		full_idea_group = aristocracy_ideas
		full_idea_group = economic_ideas
	}
		
	global_trade_goods_size_modifier = 0.05
	prestige = 1.0
		
	ai_will_do = {
		factor = 1	
	}
}





formalized_measures = {

	monarch_power = ADM


	potential = {
		has_idea_group = plutocracy_ideas
		has_idea_group = economic_ideas
		OR = {
			full_idea_group = plutocracy_ideas
			full_idea_group = economic_ideas
		}
	}
	
	allow = {
		full_idea_group = plutocracy_ideas
		full_idea_group = economic_ideas
	}	

	global_tax_modifier = 0.10
	republican_tradition = 0.2

	ai_will_do = {
		factor = 1
	}
}



agricultural_cultivations = {
	monarch_power = ADM

	potential = {
		has_idea_group = economic_ideas
		has_idea_group = quantity_ideas
		OR = {
			full_idea_group = economic_ideas
			full_idea_group = quantity_ideas
		}
	}
	
	allow = {
		full_idea_group = economic_ideas
		full_idea_group = quantity_ideas
	}		
	
	land_maintenance_modifier = -0.05
	naval_maintenance_modifier = -0.1

	ai_will_do = {
		factor = 1	
	}

}







naval_school_act = {

	monarch_power = ADM


	potential = {
		has_idea_group = administrative_ideas
		has_idea_group = maritime_ideas
		OR = {
			full_idea_group = administrative_ideas
			full_idea_group = maritime_ideas
		}
	}
	
	allow = {
		full_idea_group = administrative_ideas
		full_idea_group = maritime_ideas
	}

	trade_efficiency = 0.05
	global_ship_repair = 0.2

	ai_will_do = {
		factor = 1	
	}

}
	


the_espionage_act = {
	monarch_power = ADM


	potential = {
		has_idea_group = innovativeness_ideas
		has_idea_group = spy_ideas
		OR = {
			full_idea_group = innovativeness_ideas
			full_idea_group = spy_ideas
		}
	}
	
	allow = {
		full_idea_group = innovativeness_ideas
		full_idea_group = spy_ideas
	}

	global_spy_defence = 0.33
	spy_offence = 0.50
	
	
	ai_will_do = {
		factor = 1
	}
}



noble_loyalty_act = {
	monarch_power = ADM


	potential = {
		has_idea_group = aristocracy_ideas
		has_idea_group = spy_ideas
		OR = {
			full_idea_group = aristocracy_ideas
			full_idea_group = spy_ideas
		}
	}
	
	allow = {
		full_idea_group = aristocracy_ideas
		full_idea_group = spy_ideas
	}

	manpower_recovery_speed = 0.10
	cavalry_power = 0.10
	
	ai_will_do = {
		factor = 1
	}
}

manueline_architecture = {
	monarch_power = ADM


	potential = {
		has_idea_group = aristocracy_ideas
		has_idea_group = expansion_ideas
		OR = {
			full_idea_group = aristocracy_ideas
			full_idea_group = expansion_ideas
		}
	}
	
	allow = {
		full_idea_group = aristocracy_ideas
		full_idea_group = expansion_ideas
	}

	build_cost = -0.10
	
	ai_will_do = {
		factor = 1
	}
}
 

 
the_foreign_support_act = {
	monarch_power = ADM


	potential = {
		has_idea_group = spy_ideas
		has_idea_group = economic_ideas
		OR = {
			full_idea_group = spy_ideas
			full_idea_group = economic_ideas
		}
	}
	
	allow = {
		full_idea_group = spy_ideas
		full_idea_group = economic_ideas
	}

	spy_offence = 0.33
	diplomats = 1
	
	ai_will_do = {
		factor = 1
	}
} 




deserteur_act = {
	monarch_power = ADM


	potential = {
		has_idea_group = spy_ideas
		has_idea_group = quality_ideas
		OR = {
			full_idea_group = spy_ideas
			full_idea_group = quality_ideas
		}
	}
	
	allow = {
		full_idea_group = spy_ideas
		full_idea_group = quality_ideas
	}

	army_tradition_decay = -0.01
	
	ai_will_do = {
		factor = 1
	}
}



the_royal_commission_act = {
	monarch_power = ADM


	potential = {
		has_idea_group = spy_ideas
		has_idea_group = administrative_ideas
		OR = {
			full_idea_group = spy_ideas
			full_idea_group = administrative_ideas
		}
	}
	
	allow = {
		full_idea_group = spy_ideas
		full_idea_group = administrative_ideas
	}

	mercenary_discipline = 0.025
	merc_maintenance_modifier = -0.15
	
	ai_will_do = {
		factor = 1
	}
}

battlefield_sermons = {
	monarch_power = ADM


	potential = {
		has_idea_group = defensive_ideas
		has_idea_group = religious_ideas
		OR = {
			full_idea_group = defensive_ideas
			full_idea_group = religious_ideas
		}
	}
	
	allow = {
		full_idea_group = defensive_ideas
		full_idea_group = religious_ideas
	}

	shock_damage_received = -0.05
	global_manpower_modifier = 0.1

	ai_will_do = {
		factor = 1
	}

}

public_welfare_act = {
	monarch_power = ADM


	potential = {
		has_idea_group = defensive_ideas
		has_idea_group = economic_ideas
		OR = {
			full_idea_group = defensive_ideas
			full_idea_group = economic_ideas
		}
	}
	
	allow = {
		full_idea_group = defensive_ideas
		full_idea_group = economic_ideas
	}

	war_exhaustion = -0.03
	hostile_attrition = 0.5

	ai_will_do = {
		factor = 1	
	}
}

the_spy_discovery_act = {
	monarch_power = ADM


	potential = {
		has_idea_group = defensive_ideas
		has_idea_group = administrative_ideas
		OR = {
			full_idea_group = defensive_ideas
			full_idea_group = administrative_ideas
		}
	}
	
	allow = {
		full_idea_group = defensive_ideas
		full_idea_group = administrative_ideas
	}

	yearly_corruption = -0.1
	global_spy_defence = 0.2

	ai_will_do = {
		factor = 1
	}
}


enforce_religious_law = {
	monarch_power = ADM


	potential = {
		has_idea_group = religious_ideas
		has_idea_group = spy_ideas
		OR = {
			full_idea_group = religious_ideas
			full_idea_group = spy_ideas
		}
	}
	
	allow = {
		full_idea_group = religious_ideas
		full_idea_group = spy_ideas
	}

	global_missionary_strength = 0.02
	tolerance_own = 1

	ai_will_do = {
		factor = 1
	}
}

the_tolerance_act = {
	monarch_power = ADM

	potential = {
		has_idea_group = religious_ideas
		has_idea_group = plutocracy_ideas
		OR = {
			full_idea_group = religious_ideas
			full_idea_group = plutocracy_ideas
		}
	}
	
	allow = {
		full_idea_group = religious_ideas
		full_idea_group = plutocracy_ideas
	}

	development_cost = -0.10

	ai_will_do = {
		factor = 1			
	}
}

public_legitimacy = {
	monarch_power = ADM

	potential = {
		has_idea_group = spy_ideas
		has_idea_group = plutocracy_ideas
		OR = {
			full_idea_group = spy_ideas
			full_idea_group = plutocracy_ideas
		}
	}
	
	allow = {
		full_idea_group = spy_ideas
		full_idea_group = plutocracy_ideas
	}

	global_unrest = -1
	global_spy_defence = 0.2

	ai_will_do = {
		factor = 1	
	}
}


km_munition_factory = {
	monarch_power = ADM

	potential = {
		has_idea_group = offensive_ideas
		has_idea_group = administrative_ideas
		OR = {
			full_idea_group = offensive_ideas
			full_idea_group = administrative_ideas
		}
	}
	
	allow = {
		full_idea_group = offensive_ideas
		full_idea_group = administrative_ideas
	}

	artillery_cost = -0.10
	regiment_recruit_speed = -0.25

	ai_will_do = {
		factor = 1
	}
}

the_stamp_act = {
	monarch_power = ADM
	
	potential = {
		has_idea_group = economic_ideas
		has_idea_group = exploration_ideas
		OR = {
			full_idea_group = economic_ideas
			full_idea_group = exploration_ideas
		}	
	}
	allow = {
		full_idea_group = economic_ideas
		full_idea_group = exploration_ideas
	}
	
	global_tariffs = 0.2
	
	ai_will_do = {
		factor = 1		
	}
}

weapon_quality_standards = {
	monarch_power = ADM
	
	potential = {
		has_idea_group = economic_ideas
		has_idea_group = quality_ideas
		OR = {
			full_idea_group = economic_ideas
			full_idea_group = quality_ideas
		}	
	}
	allow = {
		full_idea_group = economic_ideas
		full_idea_group = quality_ideas
	}
	
	
	land_attrition = -0.10
	naval_attrition = -0.15

	ai_will_do = {
		factor = 1
	}
}


colonial_administration_act = {
	monarch_power = ADM
	
	potential = {
		has_idea_group = administrative_ideas
		has_idea_group = exploration_ideas
		OR = {
			full_idea_group = administrative_ideas
			full_idea_group = exploration_ideas
		}	
	}
	allow = {
		full_idea_group = administrative_ideas
		full_idea_group = exploration_ideas
	}
	
	colonists = 1

	
	ai_will_do = {
		factor = 1
	}
}



record_keeping_act = {
	monarch_power = ADM
	potential = {
		has_idea_group = administrative_ideas
		has_idea_group = diplomatic_ideas
		OR = {
			full_idea_group = administrative_ideas
			full_idea_group = diplomatic_ideas
		}
	}
	
	allow = {
		full_idea_group = administrative_ideas
		full_idea_group = diplomatic_ideas
	}

	administrative_efficiency = 0.05
	diplomatic_annexation_cost = -0.15

	ai_will_do = {
		factor = 1
	}
}



risk_assessment_policy = {
	monarch_power = ADM

	potential = {
		has_idea_group = offensive_ideas
		has_idea_group = expansion_ideas
		OR = {
			full_idea_group = offensive_ideas
			full_idea_group = expansion_ideas
		}
	}
	
	allow = {
		full_idea_group = offensive_ideas
		full_idea_group = expansion_ideas
	}

	land_forcelimit_modifier = 0.15
	
	ai_will_do = {
		factor = 1
	}	
}


land_inheritance_act = {

	monarch_power = ADM

	potential = {
		has_idea_group = administrative_ideas
		has_idea_group = plutocracy_ideas
		OR = {
			full_idea_group = administrative_ideas
			full_idea_group = plutocracy_ideas
		}
	}
	
	allow = {
		full_idea_group = administrative_ideas
		full_idea_group = plutocracy_ideas
	}

	global_tax_modifier = 0.15
	
	
	ai_will_do = {
		factor = 1
	}
}




knights_examplar = {

	monarch_power = ADM

	potential = {
		has_idea_group = religious_ideas
		has_idea_group = aristocracy_ideas
		OR = {
			full_idea_group = religious_ideas
			full_idea_group = aristocracy_ideas
		}
	}
	
	allow = {
		full_idea_group = religious_ideas
		full_idea_group = aristocracy_ideas
	}
	
	shock_damage = 0.05
	global_unrest = -1

	ai_will_do = {
		factor = 1
	}
}


the_education_act = {

	monarch_power = ADM

	potential = {
		has_idea_group = innovativeness_ideas
		has_idea_group = plutocracy_ideas
		OR = {
			full_idea_group = innovativeness_ideas
			full_idea_group = plutocracy_ideas
		}
	}
	
	allow = {
		full_idea_group = innovativeness_ideas
		full_idea_group = plutocracy_ideas
	}

	prestige = 0.5
	idea_cost = -0.1

	ai_will_do = {
		factor = 1
	}
}


policy_of_calculate_delay = {
	monarch_power = ADM
	potential = {
		has_idea_group = religious_ideas
		has_idea_group = diplomatic_ideas
		OR = {
			full_idea_group = religious_ideas
			full_idea_group = diplomatic_ideas
		}
	}
	
	allow = {
		full_idea_group = religious_ideas
		full_idea_group = diplomatic_ideas
	}
	
	liberty_desire = -10
	embracement_cost = -0.2
 

	ai_will_do = {
		factor = 1	
	}
}


black_chamber_act = {
	monarch_power = ADM
	potential = {
		has_idea_group = spy_ideas
		has_idea_group = expansion_ideas
		OR = {
			full_idea_group = spy_ideas
			full_idea_group = expansion_ideas
		}
	}
	
	allow = {
		full_idea_group = spy_ideas
		full_idea_group = expansion_ideas
	}
	
	administrative_efficiency = 0.1
	global_spy_defence = 0.25
 

	ai_will_do = {
		factor = 1
	}
}




exchange_of_ideas = {
	monarch_power = ADM
	
	potential = {
		has_idea_group = humanist_ideas
		has_idea_group = trade_ideas
		OR = {
			full_idea_group = humanist_ideas
			full_idea_group = trade_ideas
		}
	}
	
	allow = {
		full_idea_group = humanist_ideas
		full_idea_group = trade_ideas
	}	
	
	embracement_cost = -0.20
	global_institution_spread = 0.25
	
	ai_will_do = {
		factor = 1
	}
}

vassal_obligations_act = {
	monarch_power = ADM
	
	potential = {
		has_idea_group = mercantilist_ideas
		has_idea_group = economic_ideas
		OR = {
			full_idea_group = mercantilist_ideas
			full_idea_group = economic_ideas
		}
	}
	
	allow = {
		full_idea_group = mercantilist_ideas
		full_idea_group = economic_ideas
	}	
	
	
	trade_efficiency = 0.075
	
	ai_will_do = {
		factor = 1		
	}
}

foreign_advisors = {
	monarch_power = ADM
	
	potential = {
		has_idea_group = mercantilist_ideas
		has_idea_group = innovativeness_ideas
		OR = {
			full_idea_group = mercantilist_ideas
			full_idea_group = innovativeness_ideas
		}
	}
	
	allow = {
		full_idea_group = mercantilist_ideas
		full_idea_group = innovativeness_ideas
	}	
	
	production_efficiency = 0.075
	
	
	ai_will_do = {
		factor = 1
	}
}

vassal_integration_act = {
	monarch_power = ADM
	
	potential = {
		has_idea_group = mercantilist_ideas
		has_idea_group = administrative_ideas
		OR = {
			full_idea_group = mercantilist_ideas
			full_idea_group = administrative_ideas
		}
	}
	
	allow = {
		full_idea_group = mercantilist_ideas
		full_idea_group = administrative_ideas
	}	
	
	merchants = 1
	
	ai_will_do = {
		factor = 1	
	}
	
}

overseas_dominions = {
	monarch_power = ADM
	
	potential = {
		has_idea_group = mercantilist_ideas
		has_idea_group = expansion_ideas
		OR = {
			full_idea_group = mercantilist_ideas
			full_idea_group = expansion_ideas
		}
	}
	
	allow = {
		full_idea_group = mercantilist_ideas
		full_idea_group = expansion_ideas
	}	
	
	center_of_trade_upgrade_cost = -0.33
	
	ai_will_do = {
		factor = 1			
	}
}

enlightened_aristocracy = {
	monarch_power = ADM
	
	potential = {
		has_idea_group = humanist_ideas
		has_idea_group = aristocracy_ideas
		OR = {
			full_idea_group = humanist_ideas
			full_idea_group = aristocracy_ideas
		}
	}
	
	allow = {
		full_idea_group = humanist_ideas
		full_idea_group = aristocracy_ideas
	}	
	
	production_efficiency = 0.1
	cavalry_power = 0.05
	
	ai_will_do = {
		factor = 1
	}
}

volunteer_companies = {
	monarch_power = ADM
	
	potential = {
		has_idea_group = humanist_ideas
		has_idea_group = offensive_ideas
		OR = {
			full_idea_group = humanist_ideas
			full_idea_group = offensive_ideas
		}
	}
	
	allow = {
		full_idea_group = humanist_ideas
		full_idea_group = offensive_ideas
	}	
	
	manpower_recovery_speed = 0.2
	
	ai_will_do = {
		factor = 1
	}
}

visionary_thinkers = {
	monarch_power = ADM
	
	potential = {
		has_idea_group = humanist_ideas
		has_idea_group = quality_ideas
		OR = {
			full_idea_group = humanist_ideas
			full_idea_group = quality_ideas
		}
	}
	
	allow = {
		full_idea_group = humanist_ideas
		full_idea_group = quality_ideas
	}	
	
	technology_cost = -0.05
	prestige_decay = -0.005
	
	ai_will_do = {
		factor = 1
	}
}


cultural_recognition_act = {
	monarch_power = ADM
	
	potential = {
		has_idea_group = humanist_ideas
		has_idea_group = plutocracy_ideas
		OR = {
			full_idea_group = humanist_ideas
			full_idea_group = plutocracy_ideas
		}
	}
	
	allow = {
		full_idea_group = humanist_ideas
		full_idea_group = plutocracy_ideas
	}	
	
	global_manpower_modifier = 0.1
	global_unrest = -1
	
	ai_will_do = {
		factor = 1
	}
}