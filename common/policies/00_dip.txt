thought_control = {
	monarch_power = DIP

		potential = {
		has_idea_group = theocratic_ideas
		has_idea_group = mercantilist_ideas
		OR = {
			full_idea_group = theocratic_ideas
			full_idea_group = mercantilist_ideas
		}	
	}
	allow = {
		full_idea_group = theocratic_ideas
		full_idea_group = mercantilist_ideas
	}
	
	naval_morale = 0.05
	tolerance_own = 1
	
	ai_will_do = {
		factor = 1	
	}
}	

prof_mari = {
	monarch_power = DIP

		potential = {
		has_idea_group = professional_army_ideas
		has_idea_group = maritime_ideas
		OR = {
			full_idea_group = professional_army_ideas
			full_idea_group = maritime_ideas
		}	
	}
	allow = {
		full_idea_group = professional_army_ideas
		full_idea_group = maritime_ideas
	}
	
	ship_durability = 0.05
	naval_morale = 0.05
	
	ai_will_do = {
		factor = 1	
	}
}

prof_trade = {
	monarch_power = DIP

		potential = {
		has_idea_group = professional_army_ideas
		has_idea_group = trade_ideas
		OR = {
			full_idea_group = professional_army_ideas
			full_idea_group = trade_ideas
		}	
	}
	allow = {
		full_idea_group = professional_army_ideas
		full_idea_group = trade_ideas
	}
	
	reinforce_cost_modifier = -0.25
	reinforce_speed = 0.25
	
	ai_will_do = {
		factor = 1	
	}
}

prof_explo = {
	monarch_power = DIP

		potential = {
		has_idea_group = professional_army_ideas
		has_idea_group = exploration_ideas
		OR = {
			full_idea_group = professional_army_ideas
			full_idea_group = exploration_ideas
		}	
	}
	allow = {
		full_idea_group = professional_army_ideas
		full_idea_group = exploration_ideas
	}
	
	global_colonial_growth = 10
	native_uprising_chance = -0.25
	
	ai_will_do = {
		factor = 1	
	}
}

prof_esp = {
	monarch_power = DIP

		potential = {
		has_idea_group = professional_army_ideas
		has_idea_group = spy_ideas
		OR = {
			full_idea_group = professional_army_ideas
			full_idea_group = spy_ideas
		}	
	}
	allow = {
		full_idea_group = professional_army_ideas
		full_idea_group = spy_ideas
	}
	
	movement_speed = 0.1
	land_attrition = -0.1
	
	ai_will_do = {
		factor = 1	
	}
}

prof_dip = {
	monarch_power = DIP

		potential = {
		has_idea_group = professional_army_ideas
		has_idea_group = diplomatic_ideas
		OR = {
			full_idea_group = professional_army_ideas
			full_idea_group = diplomatic_ideas
		}	
	}
	allow = {
		full_idea_group = professional_army_ideas
		full_idea_group = diplomatic_ideas
	}
	
	improve_relation_modifier = 0.15
	diplomatic_reputation = 1
	
	ai_will_do = {
		factor = 1	
	}
}

prof_merc = {
	monarch_power = DIP

		potential = {
		has_idea_group = professional_army_ideas
		has_idea_group = mercantilist_ideas
		OR = {
			full_idea_group = professional_army_ideas
			full_idea_group = mercantilist_ideas
		}	
	}
	allow = {
		full_idea_group = professional_army_ideas
		full_idea_group = mercantilist_ideas
	}
	
	infantry_cost = -0.15
	
	ai_will_do = {
		factor = 1	
	}
}

blessed_figureheads = {
	monarch_power = DIP

		potential = {
		has_idea_group = theocratic_ideas
		has_idea_group = maritime_ideas
		OR = {
			full_idea_group = theocratic_ideas
			full_idea_group = maritime_ideas
		}	
	}
	allow = {
		full_idea_group = theocratic_ideas
		full_idea_group = maritime_ideas
	}
	
	naval_morale = 0.10
	diplomatic_reputation = 1
	ai_will_do = {
		factor = 1	
	}
}

rogues_second_chance = {
	monarch_power = DIP

		potential = {
		has_idea_group = theocratic_ideas
		has_idea_group = spy_ideas
		OR = {
			full_idea_group = theocratic_ideas
			full_idea_group = spy_ideas
		}	
	}
	allow = {
		full_idea_group = theocratic_ideas
		full_idea_group = spy_ideas
	}
	
	defensiveness = 0.15
        devotion = 1

	ai_will_do = {
		factor = 1	
	}
}

authority_via_charity = {
	monarch_power = DIP

		potential = {
		has_idea_group = theocratic_ideas
		has_idea_group = diplomatic_ideas
		OR = {
			full_idea_group = theocratic_ideas
			full_idea_group = diplomatic_ideas
		}	
	}
	allow = {
		full_idea_group = theocratic_ideas
		full_idea_group = diplomatic_ideas
	}
	
	tolerance_own = 1
	diplomatic_upkeep = 1
	papal_influence = 1
	ai_will_do = {
		factor = 1	
	}
}

the_statute_of_monopolies = {

	monarch_power = DIP

	potential = {
		has_idea_group = trade_ideas
		has_idea_group = economic_ideas
		OR = {
			full_idea_group = trade_ideas
			full_idea_group = economic_ideas
		}
	}
	
	allow = {
		full_idea_group = trade_ideas
		full_idea_group = economic_ideas
	}

	interest = -1.0
	inflation_reduction = 0.05


	ai_will_do = {
		factor = 1
	}
}


the_recruiting_act = {
	monarch_power = DIP

	potential = {
		has_idea_group = maritime_ideas
		has_idea_group = economic_ideas
		OR = {
			full_idea_group = maritime_ideas
			full_idea_group = economic_ideas
		}
	}
	
	allow = {
		full_idea_group = maritime_ideas
		full_idea_group = economic_ideas
	}
	
	naval_maintenance_modifier = -0.15
	production_efficiency = 0.05
	
	ai_will_do = {
		factor = 1
	}	
}



convoy_system = {

	monarch_power = DIP

	potential = {
		has_idea_group = maritime_ideas
		has_idea_group = offensive_ideas
		OR = {
			full_idea_group = maritime_ideas
			full_idea_group = offensive_ideas
		}
	}
	
	allow = {
		full_idea_group = maritime_ideas
		full_idea_group = offensive_ideas
	}
	
	trade_efficiency = 0.1
	ship_durability = 0.05
	
	ai_will_do = {
		factor = 1
	}
}

the_dissolution_act = {

	monarch_power = DIP

	potential = {
		has_idea_group = diplomatic_ideas
		has_idea_group = economic_ideas
		OR = {
			full_idea_group = diplomatic_ideas
			full_idea_group = economic_ideas
		}
	}
	
	allow = {
		full_idea_group = diplomatic_ideas
		full_idea_group = economic_ideas
	}
	
	global_tax_modifier = 0.1
	development_cost = -0.05

	ai_will_do = {
		factor = 1	
	}
}






the_wooden_wall = {

	monarch_power = DIP

	potential = {
		has_idea_group = maritime_ideas
		has_idea_group = defensive_ideas
		OR = {
			full_idea_group = maritime_ideas
			full_idea_group = defensive_ideas
		}
	}
	
	allow = {
		full_idea_group = maritime_ideas
		full_idea_group = defensive_ideas
	}
	
	naval_morale = 0.10
	global_ship_cost = -0.10
	
	ai_will_do = {
		factor = 1		
	}
}








the_importation_act = {

	monarch_power = DIP

	potential = {
		has_idea_group = trade_ideas
		has_idea_group = administrative_ideas
		OR = {
			full_idea_group = trade_ideas
			full_idea_group = administrative_ideas
		}	
	}
	allow = {
		full_idea_group = trade_ideas
		full_idea_group = administrative_ideas
	}
	
	global_trade_power = 0.15
	
	ai_will_do = {
		factor = 1
	}
}

colonial_expansions = {

	monarch_power = DIP

	potential = {
		has_idea_group = expansion_ideas
		has_idea_group = exploration_ideas
		OR = {
			full_idea_group = expansion_ideas
			full_idea_group = exploration_ideas
		}
	}
	
	allow = {
		full_idea_group = expansion_ideas
		full_idea_group = exploration_ideas
	}		

	global_colonial_growth = 20
	
	ai_will_do = {
		factor = 1
	}
}


beneficial_neglect = {

	monarch_power = DIP


	potential = {
		has_idea_group = exploration_ideas
		has_idea_group = innovativeness_ideas
		OR = {
			full_idea_group = exploration_ideas
			full_idea_group = innovativeness_ideas
		}
	}
	
	allow = {
		full_idea_group = exploration_ideas
		full_idea_group = innovativeness_ideas
	}

	colonist_placement_chance = 0.05
	range = 0.5


	ai_will_do = {
		factor = 1
	}
}



restrictions_for_colonies = {
	monarch_power = DIP

	potential = {
		has_idea_group = exploration_ideas
		has_idea_group = religious_ideas
		OR = {
			full_idea_group = exploration_ideas
			full_idea_group = religious_ideas
		}
	}
	
	allow = {
		full_idea_group = exploration_ideas
		full_idea_group = religious_ideas
	}



	global_missionary_strength = 0.02
	naval_morale = 0.05

	ai_will_do = {
		factor = 1		
	}
}




naval_infrastructure_act = {
	monarch_power = DIP


	potential = {
		has_idea_group = maritime_ideas
		has_idea_group = expansion_ideas
		OR = {
			full_idea_group = maritime_ideas
			full_idea_group = expansion_ideas
		}
	}
	
	allow = {
		full_idea_group = maritime_ideas
		full_idea_group = expansion_ideas
	}


	naval_forcelimit_modifier = 0.30
	global_tariffs = 0.10


	ai_will_do = {
		factor = 1				
	}
}

diplomatic_cooperation_act = {
	monarch_power = DIP


	potential = {
		has_idea_group = innovativeness_ideas
		has_idea_group = diplomatic_ideas
		OR = {
			full_idea_group = innovativeness_ideas
			full_idea_group = diplomatic_ideas
		}
	}
	
	allow = {
		full_idea_group = innovativeness_ideas
		full_idea_group = diplomatic_ideas
	}


	advisor_cost = -0.10
	global_institution_spread = 0.10


	ai_will_do = {
		factor = 1
	}
}



the_banking_system = {
	monarch_power = DIP


	potential = {
		has_idea_group = innovativeness_ideas
		has_idea_group = trade_ideas
		OR = {
			full_idea_group = innovativeness_ideas
			full_idea_group = trade_ideas
		}
	}
	
	allow = {
		full_idea_group = innovativeness_ideas
		full_idea_group = trade_ideas
	}

	trade_steering = 0.15
	trade_efficiency = 0.075


	ai_will_do = {
		factor = 1
	}
}

new_naval_tactis = {
	monarch_power = DIP
	potential = {
		has_idea_group = innovativeness_ideas
		has_idea_group = maritime_ideas
		OR = {
			full_idea_group = innovativeness_ideas
			full_idea_group = maritime_ideas
		}
	}
	
	allow = {
		full_idea_group = innovativeness_ideas
		full_idea_group = maritime_ideas
	}

	leader_naval_manuever = 1
	global_ship_trade_power = 0.05


	ai_will_do = {
		factor = 1
	}
}


trade_connection_policy = {
	monarch_power = DIP
	potential = {
		has_idea_group = aristocracy_ideas
		has_idea_group = trade_ideas
		OR = {
			full_idea_group = aristocracy_ideas
			full_idea_group = trade_ideas
		}
	}
	
	allow = {
		full_idea_group = aristocracy_ideas
		full_idea_group = trade_ideas
	}

	global_foreign_trade_power = 0.1
	merchants = 1

	ai_will_do = {
		factor = 1		
	}
}

nobility_embassy_act = {
	monarch_power = DIP
	potential = {
		has_idea_group = aristocracy_ideas
		has_idea_group = exploration_ideas
		OR = {
			full_idea_group = aristocracy_ideas
			full_idea_group = exploration_ideas
		}
	}
	
	allow = {
		full_idea_group = aristocracy_ideas
		full_idea_group = exploration_ideas
	}

	global_manpower_modifier = 0.20

	ai_will_do = {
		factor = 1
	}
}

nobles_in_navy_act = {
	monarch_power = DIP
	potential = {
		has_idea_group = aristocracy_ideas
		has_idea_group = maritime_ideas
		OR = {
			full_idea_group = aristocracy_ideas
			full_idea_group = maritime_ideas
		}
	}
	
	allow = {
		full_idea_group = aristocracy_ideas
		full_idea_group = maritime_ideas
	}

	galley_cost = -0.10
	galley_power = 0.10
	navy_tradition = 1

	ai_will_do = {
		factor = 1
	}
}



naval_secrecy_act = {
	monarch_power = DIP
	potential = {
		has_idea_group = exploration_ideas
		has_idea_group = defensive_ideas
		OR = {
			full_idea_group = exploration_ideas
			full_idea_group = defensive_ideas
		}
	}
	
	allow = {
		full_idea_group = exploration_ideas
		full_idea_group = defensive_ideas
	}

	global_spy_defence = 0.2
	land_attrition = -0.10

	ai_will_do = {
		factor = 1
	}
}



streamlined_galley_production = {
	monarch_power = DIP
	potential = {
		has_idea_group = maritime_ideas
		has_idea_group = quantity_ideas
		OR = {
			full_idea_group = maritime_ideas
			full_idea_group = quantity_ideas
		}
	}
	
	allow = {
		full_idea_group = maritime_ideas
		full_idea_group = quantity_ideas
	}
	
	global_sailors_modifier = 0.15
	sailors_recovery_speed = 0.10

	ai_will_do = {
		factor = 1		
	}	

}

cloth_quality_edict = {
	monarch_power = DIP
	potential = {
		has_idea_group = trade_ideas
		has_idea_group = quality_ideas
		OR = {
			full_idea_group = trade_ideas
			full_idea_group = quality_ideas
		}
	}
	
	allow = {
		full_idea_group = trade_ideas
		full_idea_group = quality_ideas
	}
	
	light_ship_cost = -0.10
	light_ship_power = 0.20

	ai_will_do = {
		factor = 1
	}
}



production_quota_act = {
	monarch_power = DIP
	potential = {
		has_idea_group = trade_ideas
		has_idea_group = quantity_ideas
		OR = {
			full_idea_group = trade_ideas
			full_idea_group = quantity_ideas
		}
	}
	
	allow = {
		full_idea_group = trade_ideas
		full_idea_group = quantity_ideas
	}
	
	sailors_recovery_speed = 0.20
	naval_forcelimit_modifier = 0.25 

	ai_will_do = {
		factor = 1
	}
}

encouragement_of_merchant_navy = {
	monarch_power = DIP
	potential = {
		has_idea_group = trade_ideas
		has_idea_group = expansion_ideas
		OR = {
			full_idea_group = trade_ideas
			full_idea_group = expansion_ideas
		}
	}
	
	allow = {
		full_idea_group = trade_ideas
		full_idea_group = expansion_ideas
	}
	
	trade_range_modifier = 0.25
	range = 0.5
 

	ai_will_do = {
		factor = 1
	}
}



commercial_embassies = {
	monarch_power = DIP
	potential = {
		has_idea_group = diplomatic_ideas
		has_idea_group = expansion_ideas
		OR = {
			full_idea_group = diplomatic_ideas
			full_idea_group = expansion_ideas
		}
	}
	
	allow = {
		full_idea_group = diplomatic_ideas
		full_idea_group = expansion_ideas
	}
	
	global_trade_power = 0.10

	ai_will_do = {
		factor = 1	
	}
}



trade_kontor_network = {
	monarch_power = DIP
	potential = {
		has_idea_group = plutocracy_ideas
		has_idea_group = trade_ideas
		OR = {
			full_idea_group = plutocracy_ideas
			full_idea_group = trade_ideas
		}
	}
	
	allow = {
		full_idea_group = plutocracy_ideas
		full_idea_group = trade_ideas
	}
	
	global_own_trade_power = 0.15
	global_ship_cost = -0.1

	ai_will_do = {
		factor = 1
	}
}



policy_of_neutrality = {
	monarch_power = DIP
	potential = {
		has_idea_group = diplomatic_ideas
		has_idea_group = plutocracy_ideas
		OR = {
			full_idea_group = diplomatic_ideas
			full_idea_group = plutocracy_ideas
		}
	}
	
	allow = {
		full_idea_group = diplomatic_ideas
		full_idea_group = plutocracy_ideas
	}
	
	global_unrest = -1
	republican_tradition = 0.2
	

	ai_will_do = {
		factor = 1
	}
}


professional_diplomatic_corps = {
	monarch_power = DIP
	potential = {
		has_idea_group = diplomatic_ideas
		has_idea_group = quality_ideas
		OR = {
			full_idea_group = diplomatic_ideas
			full_idea_group = quality_ideas
		}
	}
	
	allow = {
		full_idea_group = diplomatic_ideas
		full_idea_group = quality_ideas
	}
	
	diplomatic_reputation = 1
	global_institution_spread = 0.10
	envoy_travel_time = -0.25

	ai_will_do = {
		factor = 1
	}

}



colonial_companies_act = {
	monarch_power = DIP
	potential = {
		has_idea_group = exploration_ideas
		has_idea_group = plutocracy_ideas
		OR = {
			full_idea_group = exploration_ideas
			full_idea_group = plutocracy_ideas
		}
	}
	
	allow = {
		full_idea_group = exploration_ideas
		full_idea_group = plutocracy_ideas
	}
	
	range = 0.20
	global_colonial_growth = 10

	ai_will_do = {
		factor = 1
	}
}

protected_shipping_lanes = {
	monarch_power = DIP
	potential = {
		has_idea_group = maritime_ideas
		has_idea_group = plutocracy_ideas
		OR = {
			full_idea_group = maritime_ideas
			full_idea_group = plutocracy_ideas
		}
	}
	
	allow = {
		full_idea_group = maritime_ideas
		full_idea_group = plutocracy_ideas
	}
	
	light_ship_power = 0.20

	ai_will_do = {
		factor = 1
	}
}


church_sponsored_guilds = {

	monarch_power = DIP
	potential = {
		has_idea_group = religious_ideas
		has_idea_group = trade_ideas
		OR = {
			full_idea_group = religious_ideas
			full_idea_group = trade_ideas
		}
	}
	
	allow = {
		full_idea_group = religious_ideas
		full_idea_group = trade_ideas
	}
	
	global_ship_cost = -0.1
	
	ai_will_do = {
		factor = 1			
	}
	

}


chaplains_of_the_fleet = {

	monarch_power = DIP
	potential = {
		has_idea_group = religious_ideas
		has_idea_group = maritime_ideas
		OR = {
			full_idea_group = religious_ideas
			full_idea_group = maritime_ideas
		}
	}
	
	allow = {
		full_idea_group = religious_ideas
		full_idea_group = maritime_ideas
	}
	
	naval_morale = 0.05
	recover_navy_morale_speed = 0.1
	
	ai_will_do = {
		factor = 1
	}
}



taxation_with_representation = {
	monarch_power = DIP
	potential = {
		has_idea_group = expansion_ideas
		has_idea_group = plutocracy_ideas
		OR = {
			full_idea_group = expansion_ideas
			full_idea_group = plutocracy_ideas
		}
	}
	
	allow = {
		full_idea_group = expansion_ideas
		full_idea_group = plutocracy_ideas
	}
	
	colonist_placement_chance = 0.05
	global_tariffs = 0.1

	ai_will_do = {
		factor = 1	
	}
}

multilingual_diplomats = {
	monarch_power = DIP
	
	potential = {
		has_idea_group = humanist_ideas
		has_idea_group = diplomatic_ideas
		OR = {
			full_idea_group = humanist_ideas
			full_idea_group = diplomatic_ideas
		}
	}
	
	allow = {
		full_idea_group = humanist_ideas
		full_idea_group = diplomatic_ideas
	}	
	
	technology_cost = -0.05
	global_manpower_modifier = 0.10
	
	ai_will_do = {
		factor = 1
	}
}

native_assimilation_act = {
	monarch_power = DIP
	
	potential = {
		has_idea_group = humanist_ideas
		has_idea_group = exploration_ideas
		OR = {
			full_idea_group = humanist_ideas
			full_idea_group = exploration_ideas
		}
	}
	
	allow = {
		full_idea_group = humanist_ideas
		full_idea_group = exploration_ideas
	}	
	
	global_colonial_growth = 15
	native_uprising_chance = -0.5
	
	ai_will_do = {
		factor = 1
	}
}

cultural_understanding = {
	monarch_power = DIP
	
	potential = {
		has_idea_group = humanist_ideas
		has_idea_group = spy_ideas
		OR = {
			full_idea_group = humanist_ideas
			full_idea_group = spy_ideas
		}
	}
	
	allow = {
		full_idea_group = humanist_ideas
		full_idea_group = spy_ideas
	}	
	
	rebel_support_efficiency = 0.5
	global_unrest = -2
	
	ai_will_do = {
		factor = 1
	}
}

the_foreign_aid_act = {
	monarch_power = DIP
	
	potential = {
		has_idea_group = humanist_ideas
		has_idea_group = mercantilist_ideas
		OR = {
			full_idea_group = humanist_ideas
			full_idea_group = mercantilist_ideas
		}
	}
	
	allow = {
		full_idea_group = humanist_ideas
		full_idea_group = mercantilist_ideas
	}	
	
	technology_cost = -0.1
	
	ai_will_do = {
		factor = 1
	}
}

cultural_unity = {
	monarch_power = DIP
	
	potential = {
		has_idea_group = mercantilist_ideas
		has_idea_group = religious_ideas
		OR = {
			full_idea_group = mercantilist_ideas
			full_idea_group = religious_ideas
		}
	}
	
	allow = {
		full_idea_group = mercantilist_ideas
		full_idea_group = religious_ideas
	}	
	
	global_missionary_strength = 0.01
	global_trade_goods_size_modifier = 0.05
	
	ai_will_do = {
		factor = 1
	}
}


fleet_surgeons = {
	monarch_power = DIP
	
	potential = {
		has_idea_group = humanist_ideas
		has_idea_group = maritime_ideas
		OR = {
			full_idea_group = humanist_ideas
			full_idea_group = maritime_ideas
		}
	}
	
	allow = {
		full_idea_group = humanist_ideas
		full_idea_group = maritime_ideas
	}	
	
	naval_maintenance_modifier = -0.05
	recover_navy_morale_speed = 0.10
	ship_durability = 0.05
	
	ai_will_do = {
		factor = 1
	}
}


the_integrated_administration_act = {
	monarch_power = DIP
	
	potential = {
		has_idea_group = mercantilist_ideas
		has_idea_group = quality_ideas
		OR = {
			full_idea_group = mercantilist_ideas
			full_idea_group = quality_ideas
		}
	}
	
	allow = {
		full_idea_group = mercantilist_ideas
		full_idea_group = quality_ideas
	}	
	
	trade_efficiency = 0.05
	global_prov_trade_power_modifier = 0.10
	
	ai_will_do = {
		factor = 1			
	}
}


full_sovereignty_act = {
	monarch_power = DIP
	
	potential = {
		has_idea_group = plutocracy_ideas
		has_idea_group = mercantilist_ideas
		OR = {
			full_idea_group = plutocracy_ideas
			full_idea_group = mercantilist_ideas
		}
	}
	
	allow = {
		full_idea_group = plutocracy_ideas
		full_idea_group = mercantilist_ideas
	}	
	
	caravan_power = 0.25
	placed_merchant_power = 5
	
	ai_will_do = {
		factor = 1
	}
}